## NetherTime ##
Plugin created by 

# To compile latest source code: #

Download source zip

Install Maven

Extract source to a directory

Naviagte to cmd to that directory

Type 'mvn install' for Maven to compile the source to a JAR file.

# To get latest JAR: #

Head over to Downloads tab

Download latent JAR

# Basic Instructions for Plugin #

Options are in the config.yml

The default Nether world cannot be deleted whilst the server is running. So, to circle around that, this plugin creates a different Nether which will be used as the Nether.

Fallback world is which world to 'fallback' to if players are in the Nether, and the Nether needs to reset. (e.g. Fallback to main world)

'time-to-pass' is the amount of # MILLISECONDS # (1/1000th of a second) needed to pass in order for the Nether to reset.

'timestamp' is a timestamp of when the server stopped. Upon first start up, 'timestamp' is not in the config. You can edit the timestamp.

'nether-world-name' is the name of the alternate Nether world used. It cannot be the same as your main world, fall-back-world, or your main world's 2 dimensions

# Troubleshooting #

Nether not resetting? Received a scary stacktrace? Need to 'Contact nearest dev'?

You can open an issue using the Issue Tracker of this repo. The issue will be resolved by me, or another developer.

You can try to restart the server, in an attempt to resolve Nether resetting issues.

(Alternatively, you can try to contact me via Skype if you have me)