package com.gmail.mcraftworldmc.nethertime.manager;

import java.util.Date;

public class TimeManager {
	
	private long EPOCH_OFFSET_MILLIS;
	public int TIME_NEED_PASSED_MILLIS;
	
	public TimeManager(long offset, int timeNeedPassed){
		this.EPOCH_OFFSET_MILLIS = offset;
		this.TIME_NEED_PASSED_MILLIS = timeNeedPassed;
	}
	
	public boolean determineIfPassed(){
		return new Date(EPOCH_OFFSET_MILLIS).before(new Date(System.currentTimeMillis() - TIME_NEED_PASSED_MILLIS));
	}
	
	public void setNewEpochOffset(long s){
		this.EPOCH_OFFSET_MILLIS = s;
	}
	
	public void setNewTimeOffset(int s){
		this.TIME_NEED_PASSED_MILLIS = s;
	}
}
