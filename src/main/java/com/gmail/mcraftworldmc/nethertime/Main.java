package com.gmail.mcraftworldmc.nethertime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.mcraftworldmc.nethertime.manager.TimeManager;
import com.gmail.mcraftworldmc.nethertime.timer.Tick;
import com.gmail.mcraftworldmc.nethertime.utils.Util;

public class Main extends JavaPlugin {
	
	public TimeManager MANAGER;
	
	@Override
	public void onEnable(){
		saveDefaultConfig();
		getServer().getPluginManager().registerEvents(new Listener(){
			@EventHandler
			public void onPortal(PlayerPortalEvent e){
				if(!e.getTo().getWorld().equals(Bukkit.getWorlds().get(1))) return;
				String s = Bukkit.getPluginManager().getPlugin("NetherTime").getConfig().getString("nether-world-name");
				if(Bukkit.getWorld(s) == null){
					e.setCancelled(true);
					e.getPlayer().sendMessage(ChatColor.LIGHT_PURPLE + "Nether world is resetting... Please wait a few minutes!");
					return;
				}
				e.setTo(new Location(Bukkit.getWorld(Bukkit.getPluginManager().getPlugin("NetherTime").getConfig().getString("nether-world-name")), e.getTo().getX(), e.getTo().getY(), e.getTo().getZ()));
			}
		}, this);
		
		String s = getConfig().getString("nether-world-name");
		for(World w : getServer().getWorlds()){
			if(w.getName().equalsIgnoreCase(s)){
				getLogger().severe("Custom nether world cannot be the same as vanilla nether, the end world, or your fallback world!");
				Util.deleteDirectory(getDataFolder());
				saveDefaultConfig();
			}
		}
		
		Bukkit.createWorld(new WorldCreator(s).environment(Environment.NETHER));
		
		if(getConfig().get("timestamp") == null){
			this.MANAGER = new TimeManager(System.currentTimeMillis(), getConfig().getInt("time-to-pass"));
		}else{
			try {
				this.MANAGER =  new TimeManager(new SimpleDateFormat("MMM dd yyyy HH:mm:ss").parse(getConfig().getString("timestamp")).getTime(), getConfig().getInt("time-to-pass"));
				if(this.MANAGER.determineIfPassed()){
					Util.resetNether(s, getConfig().getString("fall-back-world"));
					this.MANAGER.setNewEpochOffset(System.currentTimeMillis());
				}
			} catch (ParseException e1) {
				getLogger().severe("Unable to parse timestamp: " + e1.getMessage());
				this.MANAGER = new TimeManager(System.currentTimeMillis(), getConfig().getInt("time-to-pass"));
			}
		}
		
		
		new Tick(this).runTaskTimer(this, 60 * 20 * 60 * 5, 20 * 60 * 60 * 5);
	}
	
	@Override
	public void onDisable(){
		getConfig().set("timestamp", new SimpleDateFormat("MMM dd yyyy HH:mm:ss").format(new Date(System.currentTimeMillis())).toString());
		saveConfig();
	}
	
	
	
}
