package com.gmail.mcraftworldmc.nethertime.utils;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class Util {
	
	public static void resetNether(final String netherName, String fallbackWorldName){
		final File folder = Bukkit.getWorld(netherName).getWorldFolder();
		for(Player p : Bukkit.getWorld(netherName).getPlayers()){
			p.teleport(Bukkit.getWorld(fallbackWorldName).getSpawnLocation());
			p.sendMessage(ChatColor.GRAY + "You have been teleported to the fallback world. The Nether is resetting...");
		}
		
		Bukkit.broadcastMessage(ChatColor.BOLD.toString() + ChatColor.LIGHT_PURPLE + "THE NETHER IS RESETTING. LAG MAY BE PRESENT");
		
		Bukkit.getWorld(netherName).setKeepSpawnInMemory(false);
		
		if(Bukkit.unloadWorld(netherName, true)){
			Bukkit.getLogger().info("Successfully unloaded the Nether");
		}else{
			Bukkit.getLogger().severe("The Nether could not be unloaded. Aborting operation...");
			
			Bukkit.broadcastMessage(ChatColor.RED + "SEVERE ERROR: Nether could not be unloaded. Contact your nearest dev.");
			
			return;
		}
		
		new BukkitRunnable(){
			public void run(){
				
				deleteDirectory(folder);
				
				Bukkit.createWorld(new WorldCreator(netherName).environment(Environment.NETHER));
				
				Bukkit.broadcastMessage(ChatColor.GREEN + "Successfully resetted the Nether.");
				
			}
		}.runTaskLater(Bukkit.getPluginManager().getPlugin("NetherTime"), 20 * 30);
		
	}
	
	public static void deleteDirectory(File path){
		if(path.exists()){
			for(File f : path.listFiles()){
				if(f.isDirectory()){
					deleteDirectory(f);
				}else{
					f.delete();
				}
			}
		}
	}
}
