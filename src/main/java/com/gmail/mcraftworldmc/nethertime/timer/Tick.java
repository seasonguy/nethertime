package com.gmail.mcraftworldmc.nethertime.timer;

import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.mcraftworldmc.nethertime.Main;
import com.gmail.mcraftworldmc.nethertime.utils.Util;

public class Tick extends BukkitRunnable {
	private Main PLUGIN_INSTANCE;
	
	public Tick(Main plugin){
		this.PLUGIN_INSTANCE = plugin;
	}
	
	public void run() {
		if(PLUGIN_INSTANCE.MANAGER.determineIfPassed()){
			Util.resetNether(PLUGIN_INSTANCE.getConfig().getString("nether-world-name"), PLUGIN_INSTANCE.getConfig().getString("fall-back-world"));
			PLUGIN_INSTANCE.MANAGER.setNewEpochOffset(System.currentTimeMillis());
		}
	}
	
}
